# Wizebin Dashboard

SB Admin Bootstrap with Angular 5+

## Emulando Local
* ng serve

## Emulando Local com ambientes diferentes
* ng serve --environment=staging

--------------------------------------------------------------------------------
## Configurar Nova página

	Adicionar pagina no Json dentro do arquivo Navigation.model.ts
	Exemplo:
	{
	  'id'        : 'contasGroup',
	  'title'     : 'Contas' ,
	  'type'      : 'collapse',
	  'icon'     : 'person',
	  'children'  : [
		{
		  'id'    : 'contas',
		  'title' : 'Contas',
		  'type'  : 'item',
		  'url'   : 'pages/contas/contas'
		},
		{
		  'id'    : 'relatoriosConta',
		  'title' : 'Relatórios',
		  'type'  : 'item',
		  'url'   : 'pages/contas/relatorios'
		}
	  ]
	}

Cada pasta com subpastas deve conter o arquivo de modulo para o grupo de menus tendo exemplo como o arquivo 'produtos.module.ts'.

Os aquivos estáticos para alimentar as tabelas estão na pasta ./fuse-fake-db.
	Ex: O arquivo sigsense.ts está alimentando a tabela de dispositivos sense.

Os modais são novas paginas que devem ser referenciadas na pagina que vai chama-lo e no modulo do grupo de pastas.
	Importar em declarations e entryComponents.

## Estrutura de pastas para módulos

### - Módulo = Plural
### - Módulo = Plural

- src
	- app
		- modules
			- [module]
				- [module].module.ts
				-	[models]
				-	[services]
				- [views]
					- edit
						- [submodule]-edit.component.ts
						- [submodule]-edit.component.html
						- [submodule]-edit.component.scss
					- read
						- [submodule]-read.component.ts
						- [submodule]-read.component.html
						- [submodule]-read.component.scss
					- search
						- [submodule]-search.component.ts
						- [submodule]-search.component.html
						- [submodule]-search.component.scss
					- add
						- [submodule]-add.component.ts
						- [submodule]-add.component.html
						- [submodule]-add.component.scss
					- [action]
						- [submodule]-[action].component.ts
						- [submodule]-[action].component.html
						- [submodule]-[action].component.scss

## Módulo no plural
- src
	- app
		- modules
			- pessoas
				- pessoas.module.ts
				- [geradores]
					-	[models]
					  - gerador.model.ts
					-	[services]
					  - gerador.service.ts
					- [views]
						- edit
							- geradores-edit.component.ts
							- geradores-edit.component.html
							- geradores-edit.component.scss
						- read
							- geradores-read.component.ts
							- geradores-read.component.html
							- geradores-read.component.scss
						- search
							- geradores-search.component.ts
							- geradores-search.component.html
							- geradores-search.component.scss
						- add
							- geradores-add.component.ts
							- geradores-add.component.html
							- geradores-add.component.scss


## Módulo no plural
- src
	- app
		- modules
			- pessoas
				- pessoas.module.ts
				- [geradores]
					-	[models]
					-	[services]
					- [views]
						- edit
							- geradores-edit.component.ts
							- geradores-edit.component.html
							- geradores-edit.component.scss
						- read
							- geradores-read.component.ts
							- geradores-read.component.html
							- geradores-read.component.scss
						- search
							- geradores-search.component.ts
							- geradores-search.component.html
							- geradores-search.component.scss
						- add
							- geradores-add.component.ts
							- geradores-add.component.html
							- geradores-add.component.scss


--------------------------------------------------------------------------------
