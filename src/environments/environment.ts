export const environment = {
	production: false,
	hmr: false,
	staging: false
};

declare var require: any;

export const AppConfig = {
	serviceUrl: 'https://bootcamp-wize-api.herokuapp.com/',
	version: require('../../package.json').version
};
