import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

import { AuthHttp } from '../../core/services/auth.http';

@Injectable()
export class AuthGuard implements CanActivate {
		constructor(private router: Router, public authService: AuthHttp) {}

		canActivate() {
			if (this.authService.validUser()) {
				return true;
			}

			this.router.navigate(['/login']);
			return false;
		}
}
