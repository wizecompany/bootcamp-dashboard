import { Component, OnInit, Input } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
		selector: 'app-page-header',
		templateUrl: './page-header.component.html',
		styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
		@Input() heading: string[];
		@Input() pagesucessor: string;
		@Input() router: string;
		@Input() icon: string[];
		@Input() link: string[];
		constructor() {

		}

		ngOnInit() {
		}
}
