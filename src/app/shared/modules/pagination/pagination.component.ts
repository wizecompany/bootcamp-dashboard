import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
		selector: 'app-pagination',
		templateUrl: './pagination.component.html',
		styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
	private numPages = 0;

	private currentIndex: number;
	private reload: any;

	constructor() {

	}

	ngOnInit() {
	}

	public loadPagination(numPages, page, reload: any) {
		this.numPages = numPages;
		this.reload = reload;
		this.currentIndex = page;
	}

	private goToNext() {
		if (this.currentIndex < this.numPages) {
			this.currentIndex++;
			this.reload();
		}
	}

	private goToPrevious() {
		if (this.currentIndex > 1) {
			this.currentIndex--;
			this.reload();
		}
	}

	private goToClicked(clickedIndex: number) {
		this.currentIndex = this.getNumberLabel(clickedIndex);
		this.reload();
	}

	private goToFirst() {
		this.currentIndex = 1;
		this.reload();
	}

	private goToLast() {
		this.currentIndex = this.numPages;
		this.reload();
	}

	public getCurrentIndex() {
		return this.currentIndex;
	}

	private isSelected(number) {
		if (this.getNumberLabel(number) === this.currentIndex) {
			return 'active';
		} else {
			return '';
		}
	}

	private jumpIndex() {
		if (this.currentIndex === this.getNumberLabel(1)) {
			this.currentIndex += 5;
			if (this.currentIndex > this.numPages) {
				this.currentIndex = this.numPages;
			}
			this.reload();
		} else if (this.currentIndex === this.getNumberLabel(2)) {
			this.currentIndex += 4;
			if (this.currentIndex > this.numPages) {
				this.currentIndex = this.numPages;
			}
			this.reload();
		} else if (this.currentIndex === this.getNumberLabel(3)) {
			this.currentIndex += 3;
			if (this.currentIndex > this.numPages) {
				this.currentIndex = this.numPages;
			}
			this.reload();
		} else {
			this.currentIndex = this.numPages;
			this.reload();
		}
	}

	private getNumberLabel(labelIndex: number) {
		if (this.numPages > 5) {
			if (this.currentIndex >= 3 && this.currentIndex <= this.numPages - 3) {
				return this.currentIndex + labelIndex - 3;
			} else if (this.currentIndex < 3) {
				return labelIndex;
			} else if (this.currentIndex > this.numPages - 3) {
				return this.numPages - 5 + labelIndex;
			}
		} else {
			return labelIndex;
		}
	}

}
