import { PaginationModule } from './pagination.module';

describe('DeleteModule', () => {
	let deleteModule: PaginationModule;

	beforeEach(() => {
		deleteModule = new PaginationModule();
	});

	it('should create an instance', () => {
		expect(deleteModule).toBeTruthy();
	});
});
