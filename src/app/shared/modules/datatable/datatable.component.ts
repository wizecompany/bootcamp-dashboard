
import { Component, OnInit, Input, Output, ViewContainerRef, EventEmitter, ViewChild, ContentChild } from '@angular/core';
import { DatatableColumnComponent } from './datatable-column/datatable-column.component';
// import { ToastService } from '../../../core/services/toast.service';

@Component({
	selector: 'app-datatable',
	templateUrl: './datatable.component.html',
	styleUrls: ['./datatable.component.scss']
})

export class DatatableComponent implements OnInit {
	@ViewChild('pagination') private pagination: any;
	@ContentChild('dataTableCell') cellTemplate;

	// @Input() dataset: any[]; // dados que serao mostrados na tabela
	private _dataset: any[];
	private dataSetContext: any[];
	@Input() hasPagination = true;
	@Output() reload: EventEmitter<any> = new EventEmitter();
	@Output() datasetChange: EventEmitter<any> = new EventEmitter();
	@Input() filter: any = null;
	columns: any[] = []; // colunas da tabela
	@Input() columnSort = '';
	@Input() sortBy = '';
	paging: any = null;

	constructor(private viewContainerRef: ViewContainerRef) {	}

	ngOnInit() {
	}

	@Input() get dataset() {
		return this._dataset;
	}

	set dataset(value: any) {
		if (value.length > 0)  {
			let contextList = [];
			for (let data of value) {
				contextList.push({item: data});
			}
			this.dataSetContext = contextList;
		}
		this._dataset = value;
		this.loadPage();
		this.datasetChange.emit(this._dataset);
	}


	getContext() {

	}

	setPaging(paging) {
		if (this.paging === null) {
			this.paging = paging;
		} else {
			this.paging = paging;
			this.paging.page = this.pagination.getCurrentIndex();
		}
	}

	addColumn(column) {
		delete column.table;
		// console.log(column);
		this.columns.push(column);
	}

	// pega o valor do objeto através de uma string (caminho) => Exemplo >> foo: {f: 'bar'} --> foo.f --> return 'bar'
	getObjectValue(objeto, caminho: string) {
		if (objeto.item[caminho]) {
			return objeto.item[caminho];
		} else {
			if (caminho !== undefined) {
				return caminho.split('.').reduce(function(prev, curr) {
						return prev ? prev[curr] : null;
				}, objeto.item || this);
			}
		}
	}

	sortColumn(column) {
		if (column.sort === undefined || column.sort === null || column.sortBy !== this.columnSort) {
			column.sort = 'ASC';
		}  else {
			column.sort === 'DESC' ? column.sort = 'ASC' : column.sort = 'DESC';
		}
		this.columnSort = column.sortBy;
		this.sortBy = column.sort;
		this.emitSort();
	}

	loadPage() {
		if (this.paging !== null) {
			this.pagination.loadPagination(this.paging.pages, this.paging.page, this.reloadPage.bind(this));
		}
	}

	reloadPage() {
		this.paging.page = this.pagination.getCurrentIndex();
		delete this.paging.data;
		this.emitSort();
	}

	emitSort() {
		const paginacao = {column: this.columnSort, sort: this.sortBy, filter: this.filter, paging: this.paging};
		this.reload.emit(paginacao);
	}

}
