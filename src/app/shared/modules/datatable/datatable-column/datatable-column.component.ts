
import { Component, OnInit, Input, ContentChild } from '@angular/core';
import { DatatableComponent } from '../datatable.component';
// import { ToastService } from '../../../core/services/toast.service';

@Component({
	selector: 'app-datatable-column',
	templateUrl: './datatable-column.component.html',
	styleUrls: ['./datatable-column.component.scss']
})

export class DatatableColumnComponent implements OnInit {

	@Input() header: string;
	@Input() value: any;
	@Input() sortBy: any;
	@Input() class: any;
	@Input() width: string;
	@Input() isSortable: any = false;
	@ContentChild('dataTableCell') cellTemplate;

	constructor(private table: DatatableComponent) {

	}

	ngOnInit() {
		this.sortBy ? this.sortBy = this.sortBy : this.sortBy = this.value;
		this.table.addColumn(this);
		/*this.column.class = this.class;
		this.column.isSortable = this.isSortable;
		this.column.header = this.header;
		this.column.value = this.value;
		this.column.cellTemplate = this.cellTemplate;
		this.table.addColumn(this.column);*/
		// console.log(this.column);
	}
}
