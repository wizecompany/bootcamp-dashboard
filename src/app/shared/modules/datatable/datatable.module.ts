import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatatableColumnComponent } from './datatable-column/datatable-column.component';
import { DatatableComponent } from './datatable.component';
// import { PaginationComponent } from '../pagination/pagination.component';
import { PaginationModule } from '../pagination/pagination.module';

@NgModule({
	imports: [CommonModule, PaginationModule],
	declarations: [DatatableComponent, DatatableColumnComponent],
	exports: [DatatableComponent, DatatableColumnComponent]
})

export class DatatableModule { }
