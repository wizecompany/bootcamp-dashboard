import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
	selector: '[appDoubleClick]'
})
export class DoubleClickDirective {

	constructor(
		private elemento?: ElementRef
	) { }

	@Input() stopClick: boolean;

	@HostListener('click') onclick() {
		this.tempDisable();
	}

	private tempDisable() {
		this.elemento.nativeElement.disabled = true;
		if (!this.stopClick) {
			setTimeout(() => this.elemento.nativeElement.disabled = false, 1000);
		}
	}
}
