import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoubleClickDirective } from './double-click.directive';

@NgModule({
	imports: [CommonModule],
	exports: [DoubleClickDirective],
	declarations: [DoubleClickDirective]
})
export class DoubleClickModule { }
