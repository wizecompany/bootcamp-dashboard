import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { AlertsComponent } from './alerts.component';

@NgModule({
		imports: [CommonModule, RouterModule, SweetAlert2Module.forRoot()],
		declarations: [AlertsComponent],
		exports: [AlertsComponent]
})
export class AlertsModule {}
