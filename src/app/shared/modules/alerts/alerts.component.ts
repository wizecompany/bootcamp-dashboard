import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
	selector: 'app-alert',
	templateUrl: './alerts.component.html',
	styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {
	@Input() confirmCallback: any;
	@Input() cancelCallback: any;

	@ViewChild('deleteSwal') public deleteConfirmSwal: SwalComponent;
	@ViewChild('deleteSuccessSwal') public deleteSuccessSwal: SwalComponent;
	@ViewChild('deleteErrorSwal') public deleteErrorSwal: SwalComponent;

	@ViewChild('addSuccessSwal') public addSuccessSwal: SwalComponent;
	@ViewChild('addErrorSwal') public addErrorSwal: SwalComponent;

	@ViewChild('editSwal') public editConfirmSwal: SwalComponent;
	@ViewChild('editSuccessSwal') public editSuccessSwal: SwalComponent;
	@ViewChild('editErrorSwal') public editErrorSwal: SwalComponent;

	constructor() { }

	ngOnInit() { }

	private confirm() {
		this.confirmCallback();
	}

	public showDeleteConfirm() {
		this.deleteConfirmSwal.show();
	}

	public showDeleteSuccess() {
		this.deleteSuccessSwal.show();
	}

	public showDeleteError() {
		this.deleteErrorSwal.show();
	}

	public showAddSuccess() {
		this.addSuccessSwal.show();
	}

	public showAddError() {
		this.addErrorSwal.show();
	}

	public showEditConfirm() {
		this.editConfirmSwal.show();
	}

	public showEditSuccess() {
		this.editSuccessSwal.show();
	}

	public showEditError() {
		this.editErrorSwal.show();
	}
}
