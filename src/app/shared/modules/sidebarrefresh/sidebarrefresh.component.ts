
import { Component, OnInit, Input, Output, ViewContainerRef, EventEmitter, ViewChild, ContentChild } from '@angular/core';
// import { ToastService } from '../../../core/services/toast.service';
import { SidebarService } from '../../../layout/components/services/sidebar.service';

@Component({
	selector: 'app-sidebarrefresh',
	templateUrl: './sidebarrefresh.component.html',
	styleUrls: ['./sidebarrefresh.component.scss']
})

export class SidebarRefreshComponent implements OnInit {
	@ContentChild('sidebarTemplate') template: any;
	// style
	isActive = true;
	showMenu = '';
	retratilStatus = 'retratil-open';

	private _dataset: any[];

	@Input() droppable = false;
	@Input() title = '';
	@Output() dropSuccess: EventEmitter<any> = new EventEmitter();
	@Output() datasetChange: EventEmitter<any> = new EventEmitter();

	constructor(private viewContainerRef: ViewContainerRef,
		private sidebarService: SidebarService) { }

	ngOnInit() {
		// alert('estou funcionando!');
		console.log('Temlpate', this.template);
	}

	@Input() get dataset() {
		return this._dataset;
	}

	set dataset(value: any) {
		this._dataset = value;
		this.datasetChange.emit(this._dataset);
	}

	changeRetratilStatus() {
		if (this.retratilStatus === 'retratil-open') {
			this.retratilStatus = 'retratil-close';
		} else {
			this.retratilStatus = 'retratil-open';
		}
		this.changeActive();
	}

	changeActive() {
		this.isActive = !this.isActive;
	}

	dropSuccessEmit(item) {
		this.dropSuccess.emit(item.dragData);
	}
}
