import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarRefreshComponent } from './sidebarrefresh.component';
import { SidebarService } from '../../../layout/components/services/sidebar.service';
import { DndModule } from 'ng2-dnd';

@NgModule({
	imports: [CommonModule, DndModule.forRoot()],
	declarations: [SidebarRefreshComponent],
	exports: [SidebarRefreshComponent, DndModule]
})

export class SidebarRefreshModule { }
