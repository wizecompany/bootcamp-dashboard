import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload.component';
import { FileDropModule } from 'ngx-file-drop';

@NgModule({
	imports: [CommonModule, FileDropModule],
	declarations: [UploadComponent],
	exports: [UploadComponent]
})

export class UploadModule { }
