import { Component, OnInit, Input, ViewContainerRef, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ToastService } from '../../../core/services/toast.service';
import { UploadService } from '../../../core/services/upload.service';
import { NgModel } from '@angular/forms';

@Component({
	selector: 'app-upload',
	templateUrl: './upload.component.html',
	styleUrls: ['./upload.component.scss']
})

export class UploadComponent implements OnInit {

	@Input() atributo: string;
	@Input() modelo: Object;
	@Input() readonly = false;
	@Input() label: string;
	@Output() successUpload:  EventEmitter<any> = new EventEmitter<any>();

	location = '';
	localFileName = '';

	constructor(private viewContainerRef: ViewContainerRef,
							private toastService: ToastService,
							private uploadService: UploadService,
							private ref: ChangeDetectorRef) { }

	ngOnInit() {
		if (this.modelo[this.atributo] !== undefined) {
			this.location = this.modelo[this.atributo];
		}
		if (!this.label) {
			this.label = this.atributo;
		}
	}

	upload (evento) {
		this.toastService.showLoadingToast();
		let file;
		let detectChange = false;
		if (evento.target) {
			file = evento.target.files.item(0);
			this.localFileName = file.name;
		} else {
			file = evento;
			this.localFileName = file.name;
			detectChange = true;
		}
		this.uploadService.uploadArquivo(file)
			.then(dados => {
				console.log(dados);
				this.location = dados.toString();
				console.log('location', this.location);
				this.modelo[this.atributo] = dados.toString();
				this.toastService.clearToasts();
				if (detectChange) {
					this.ref.detectChanges();
				}
				this.successUpload.emit(this.location);
				this.toastService.clearToasts();
			})
			.catch(erro => {
				this.toastService.showErrorToast('Erro', 'Erro ao realizar upload de arquivo!');
				this.removeArquivo();
			});
		}

		removeArquivo () {
			this.uploadService.toDelete.push(this.location);
			this.modelo[this.atributo] = '';
			this.location = '';
			this.localFileName = '';
			this.successUpload.emit(this.location);
	}

	getAtributo(atributo: string) {
		switch (atributo) {
			case 'arquivoFoto': {
				return 'Foto';
			}
			case 'arquivoAssinatura': {
				return 'Assinatura';
			}
			case 'arquivoCnh': {
				return 'Cnh';
			}
			case 'arquivoLicenca': {
				return 'Licença';
			}
			default: {
				return '';
			}
		}
	}

	isValid() {
		return this.location !== '';
	}

	droppedFile(evento) {
		if (evento.files.length > 1) {
			this.toastService.showErrorToast('Erro', 'Arraste apenas um arquivo!');
		} else {
			evento.files[0].fileEntry.file((file: File) => {
				if (file.type.indexOf('image/') === -1) {
					this.toastService.showErrorToast('Erro', 'Arraste uma imagem!');
				} else {
					this.upload(file);
				}
			});
		}
	}
}
