import { FormUserFeedbackComponent } from './form-user-feedback-component/form-user-feedback.component';
import { FormGroup } from '@angular/forms';
import { Directive, ElementRef, Renderer2, Input, OnChanges, HostListener, DoCheck, OnInit, ComponentRef, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';
import { FormCustomValidator } from '../../../core/services/formCustomValidator.service';

@Directive({
	selector: '[appFormUserFeedback]'
})
export class FormUserFeedbackDirective implements OnInit, DoCheck {
	@Input() formGroup: FormGroup;
	@Input() showIconError = true;
	@Input() tooltipLabel;
	formControlName: string;

	tooltip: ComponentRef<FormUserFeedbackComponent>;
	parent: any;
	iconError: any;

	constructor(
		private el: ElementRef,
		private renderer: Renderer2,
		private formCustomValidator: FormCustomValidator,
		private viewContainerRef: ViewContainerRef,
		private resolver: ComponentFactoryResolver) {
		if (!this.el.nativeElement.attributes.formcontrolname) {
			throw new Error ('É necessário definir o atributo formControlName! ');
		}
		this.formControlName = this.el.nativeElement.attributes.formcontrolname.value;
	}

	@HostListener('focus') onFocus() {
		const invalid = this.formCustomValidator.isFieldValid(this.formControlName, this.formGroup);
		if (this.tooltip || !invalid) {
			return;
		}
		if (invalid) {
			this.createTooltip();
		} else {
			this.removeTooltip();
		}
	}

	@HostListener('focusout') onFocusOut() {
		this.removeTooltip();
	}

	ngOnInit() {
		if (!this.formGroup) {
			throw new Error ('Defina o formulário deste campo');
		}
		if (!this.formGroup.get(this.formControlName)) {
			throw new Error ('Este formControl não está no FormGroup!');
		}
		this.iconError = this.createIconError();
		this.parent = this.renderer.parentNode(this.el.nativeElement);
	}

	createTooltip() {
		if (this.tooltip) {
			return;
		}
		const factory = this.resolver.resolveComponentFactory(FormUserFeedbackComponent);
		this.tooltip = this.viewContainerRef.createComponent(factory);
		let error = this.formGroup.get(this.formControlName).errors;
		if (this.tooltipLabel) {
			this.tooltip.instance.setLabel(this.tooltipLabel);
		}
		this.tooltip.instance.setErrorMessage(this.formControlName, Object.keys(error)[0]);
	}

	removeTooltip() {
		if (!this.tooltip) {
			return;
		}
		this.tooltip.destroy();
		this.tooltip = null;
	}

	ngDoCheck() {
		if (this.formGroup.get(this.formControlName).touched) {
			let invalid = this.formCustomValidator.isFieldValid(this.formControlName, this.formGroup);
			invalid ? this.addInvalidClass() : this.addValidClass();
		}
	}

	addValidClass() {
		this.renderer.removeClass(this.el.nativeElement, 'has-error');
		this.renderer.addClass(this.el.nativeElement, 'has-valid');
		this.removeTooltip();
		this.visibleIconError(false);
	}

	addInvalidClass() {
		this.renderer.removeClass(this.el.nativeElement, 'has-valid');
		this.renderer.addClass(this.el.nativeElement, 'has-error');
		this.visibleIconError(true);
		// this.createTooltip();
	}

	createIconError() {
		let icon = this.renderer.createElement('i');
		this.renderer.addClass(icon, 'inside-error-icon');
		this.renderer.addClass(icon, 'fa-info-circle');
		this.renderer.addClass(icon, 'fas');
		return icon;
	}

	visibleIconError(visible: boolean) {
		if (this.showIconError) {
			if (visible) {
				this.renderer.appendChild(this.parent, this.iconError);
			} else {
				this.renderer.removeChild(this.parent, this.iconError);
			}
		}
	}
}
