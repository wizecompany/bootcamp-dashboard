import { Component, OnInit } from '@angular/core';
import { ErrorMessage } from '../../../../core/models/errorMessage.model';

@Component({
	selector: 'app-form-user-feedback',
	templateUrl: './form-user-feedback.component.html',
	styleUrls: ['./form-user-feedback.component.scss']
})
export class FormUserFeedbackComponent implements OnInit {

	errorMessage: string;
	label: string;

	constructor(private tipMessage: ErrorMessage) { }

	ngOnInit() {
	}

	public setErrorMessage(field: string, error: string) {
		if (this.label) {
			field = this.label;
		}
		field = field.charAt(0).toUpperCase() + field.slice(1);
		const textos = {
			required: `Campo '${field}' é obrigatório!`,
			minlength: `Campo '${field}' não atingiu o limite mínimo de caracteres!`,
			maxlength: `Campo '${field}' ultrapassou o limite de caracteres permitido!`,
			pattern: `Campo '${field}' não coincide com a regra!`,
			cpfValid: `Campo cpf inválido!`,
			cnpjValid: `Campo cnpj inválido!`,
			qtdResponsaveisValid: `Deve ter ao menos um responsável cadastrado!`,
			email: `E-mail inválido`,
		};
		this.errorMessage = textos[error];
	}

	public setLabel(label) {
		this.label = label;
	}

}
