import	{	Directive,	ElementRef,	Renderer2,	OnInit	}	from	'@angular/core';

@Directive({
	selector:	'[appInputFocus]'
})
export	class	InputFocusDirective	implements	OnInit	{

	constructor(
				private	el:	ElementRef,
				private	renderer:	Renderer2
		)	{}

		ngOnInit():	void	{
		this.el.nativeElement.focus();
		}

}
