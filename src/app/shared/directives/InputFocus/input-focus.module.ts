import { CommonModule } from '@angular/common';
import { InputFocusDirective } from './input-focus.directive';
import { NgModule } from '@angular/core';


@NgModule({
		imports: [CommonModule],
		declarations: [InputFocusDirective],
		exports: [InputFocusDirective]
})

export class InputFocusModule {}
