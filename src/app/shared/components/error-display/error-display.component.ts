import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-error-display',
	templateUrl: './error-display.component.html',
	styleUrls: ['./error-display.component.scss']
})

export class ErrorDisplayComponent {
	@Input() errorMsg: string;
	@Input() displayError: boolean;
}
