import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';

import { AuthHttp } from '../core/services/auth.http';
import { ToastService } from '../core/services/toast.service';
import { StorageService } from '../core/services/storage.service';

import { AppConfig } from '../../environments/environment';

import { FormGroup, FormBuilder, Validators} from '@angular/forms';

// import { UsuarioService } from '../modules/usuarios/usuarios/services/usuario.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	animations: [routerTransition()]
})

export class LoginComponent implements OnInit {
	@ViewChild('alert') private alert: any;

	private emailField = '';
	private passwordField = '';
	private form: FormGroup;
	private form2: FormGroup;

	appVersion = '';

	private rememberMe = true;


	constructor(private formBuilder: FormBuilder, public router: Router, public route: ActivatedRoute,
		// public usuarioService: UsuarioService,
		private authService: AuthHttp, private localStorage: StorageService, public toastService: ToastService, private vrf: ViewContainerRef) {
		this.toastService.setRoot(vrf);
		this.retrieveUser();
	}

	ngOnInit() {
		this.createForm();

		this.appVersion = AppConfig.version;
	}

	private retrieveUser() {
		let usuario = JSON.parse(this.localStorage.getData('usuario'));
		if (usuario !== null) {
			this.emailField = usuario.email;
			this.passwordField = usuario.senha;
		} else {
			console.log(`localStorage sem usuario`);
		}
	}

	private login() {
		if (this.form.valid) {
			this.toastService.showLoadingToast();
			this.authService.auth(this.form.controls.email.value, this.form.controls.password.value).then((result) => {
				if (this.rememberMe) {
					this.localStorage.removeData('usuario')
					.then(() => this.localStorage.storeData('usuario', JSON.stringify(result)))
					.catch(erro => console.log(erro));
				}

				this.router.navigate(['/contatos']);
				this.toastService.clearToasts();
			}).catch((err) => {
				this.toastService.clearToasts();
				let self = this;
				setTimeout(function () {
					self.toastService.showErrorToast('Erro', 'Não foi possível logar.');
				}, 200);
				console.log('err', err);
			});
		} else {
			this.toastService.showWarningToast('Atenção', 'Erro na validação dos dados');
		}
	}

	private validateFields() {
		if (this.emailField === '' || this.passwordField === '') {
			this.toastService.showWarningToast('Atenção', 'Preencha os campos obrigatórios');
			return false;
		} else {
			return true;
		}
	}

	createForm() {
		this.form = this.formBuilder.group( {
			email:    ['', [Validators.required, Validators.email]],
			password: ['', Validators.required],
		});

		this.form2 = this.formBuilder.group( {
			recoverEmail: ['', [Validators.required, Validators.email]],
		});
	}

	public isFieldValid(field: string, form: FormGroup) {
		return !form.get(field).valid && form.get(field).touched;
	}

	public displayFieldCss(field: string, form: FormGroup) {
		return {
			'has-error': this.isFieldValid(field, form),
			'has-valid': !this.isFieldValid(field, form) && form.get(field).touched
		};
	}

	keyPress(value) {
		if (value === 13) { this.login(); }
	}

}
