import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { NgModel } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

import { AlertsModule } from '../shared';

import { ReactiveFormsModule } from '@angular/forms';
import { ErrorDisplayComponent } from './error-display/error-display.component';

@NgModule({
		imports: [CommonModule, LoginRoutingModule, FormsModule, ReactiveFormsModule, AlertsModule],
		declarations: [LoginComponent, ErrorDisplayComponent],
		providers: []
})
export class LoginModule {}
