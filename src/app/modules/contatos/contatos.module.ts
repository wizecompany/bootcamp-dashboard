import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgbCarouselModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

// Motorista
import { ContatosListComponent } from './views/list/contatos-list.component';
import { ContatosAddComponent } from './views/add/contatos-add.component';
import { ContatosEditComponent } from './views/edit/contatos-edit.component';
import { ContatosReadComponent } from './views/read/contatos-read.component';

import { ContatosService } from './services/contatos.service';

import { StatModule } from '../../shared';
import { PageHeaderModule } from '../../shared';
import { AlertsModule } from '../../shared';
import { PaginationModule } from '../../shared';
import { DatatableModule } from '../../shared';

import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ToastOptions } from 'ng2-toastr';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadModule } from '../../shared/modules/upload/upload.module';
import { TextMaskModule } from 'angular2-text-mask';

import { MaskService } from '../../core/services/masks.service';
import { UploadService } from '../../core/services/upload.service';
import { FormCustomValidator } from '../../core/services/formCustomValidator.service';

const routes: Routes = [

	{ path: '', redirectTo: 'list' },
	{ path: 'add', 	component: ContatosAddComponent },
	{ path: 'edit', 	component: ContatosEditComponent },
	{ path: 'read', 	component: ContatosReadComponent },
	{ path: 'list', component: ContatosListComponent },
];

@NgModule({
	imports: [
		RouterModule.forChild(routes),
		CommonModule,
		NgbCarouselModule.forRoot(),
		NgbAlertModule.forRoot(),
		StatModule,
		PageHeaderModule,
		ToastModule.forRoot(),
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		SweetAlert2Module.forRoot(),
		AlertsModule,
		NgbModule.forRoot(),
		UploadModule,
		TextMaskModule,
		PaginationModule,
		DatatableModule
	],
	exports: [RouterModule],
	declarations: [
		// Contatos
		ContatosListComponent,
		ContatosAddComponent,
		ContatosEditComponent,
		ContatosReadComponent
	],
	entryComponents: [
		// Contatos
		ContatosListComponent,
		ContatosAddComponent,
		ContatosEditComponent,
		ContatosReadComponent
	],
	providers: [
		ToastOptions,
		ContatosService,
		MaskService
	]
})
export class ContatosModule {}
