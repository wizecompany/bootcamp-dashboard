import { FormGroup, Validators } from '@angular/forms';
import { Validacoes } from '../../../core/services/validacoes.service';
export class Contato {
	id: string;
	nome: string;
	empresa : string;
	telefone : string;
	email : string;
	socialMedia : string;
	foto : string;
	usuario: string;

	constructor() {
	}

	fromAPI(json: any) {
		Object.assign(this, json);
	}

	toAPI() {
		let json: any = new Object();
		Object.assign(json, this);

		return json;
	}

	public fromFormGroup(form: FormGroup) {
		let value = form.value;
		this.nome = value.nome;
		this.empresa = value.empresa;
		this.telefone = value.telefone;
		this.email = value.email;
		this.socialMedia = value.socialMedia;
		this.foto = value.foto;
	}

	public toFormGroup() {
		return {
			nome: [this.nome, [Validators.required]],
			empresa: [this.empresa],
			telefone: [this.telefone, [Validators.required]],
			email: [this.email],
			socialMedia: [this.socialMedia],
			foto: [this.foto],
		};
	}
}
