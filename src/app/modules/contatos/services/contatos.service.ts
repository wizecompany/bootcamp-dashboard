import { Injectable } from '@angular/core';
import { AuthHttp } from '../../../core/services/auth.http';
import { Contato } from '../models/contatos.model';

@Injectable()
export class ContatosService {
	private baseUrl = 'contato';

	constructor(private authHttp: AuthHttp) { }

	public getBaseUrl() {
		return this.baseUrl;
	}

	public all(): Promise<any> {
		return this.authHttp.get(this.baseUrl);
	}

	public insert(model: Contato): Promise<any> {
		return this.authHttp.post(this.baseUrl, model.toAPI());
	}

	public update(model: Contato): Promise<any> {
		return this.authHttp.put(`${this.baseUrl}/${model.id}`, model.toAPI());
	}

	public delete(model: Contato): Promise<any> {
		return this.authHttp.delete(`${this.baseUrl}/${model.id}`);
	}

	public search(query: any = {}, paging: any = {}, sort: string = ''): Promise<any> {
		let data = { query: query, paging: paging, sort: sort };
		let url = `${this.baseUrl}/search`;

		return this.authHttp.post(url, data);
	}

	public getOneById(id: string): Promise<any> {
		return this.authHttp.get(`${this.baseUrl}/${id}`);
	}

	public findManyWhere(query: any = {}): Promise<any> {
		return this.authHttp.get(`${this.baseUrl}?where=${JSON.stringify(query)}`);
	}
}
