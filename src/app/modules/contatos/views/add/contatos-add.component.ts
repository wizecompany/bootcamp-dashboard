import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { FormGroup, FormBuilder} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Contato } from '../../models/contatos.model';
import { ContatosService } from '../../services/contatos.service';

import { MaskService } from '../../../../core/services/masks.service';
import { UploadService } from '../../../../core/services/upload.service';
import { FormCustomValidator } from '../../../../core/services/formCustomValidator.service';
import { AuthHttp } from '../../../../core/services/auth.http';

import { ToastService } from '../../../../core/services/toast.service';

@Component({
		selector: 'app-contatos',
		templateUrl: './contatos-add.component.html',
		styleUrls: ['./contatos-add.component.scss'],
		animations: [routerTransition()]
})
export class ContatosAddComponent implements OnInit {
	@ViewChild('alert') private alert: any;
	@ViewChild('foto') private foto: any;

	model: Contato = new Contato();

	private form: FormGroup;
	private shouldShowBadge = false;

	constructor(private formBuilder: FormBuilder,
							public router: Router,
							public route: ActivatedRoute,
							private service: ContatosService,
							public toastService: ToastService,
							private viewContainerRef: ViewContainerRef,
							public maskService: MaskService,
							private uploadService: UploadService,
							private formValidator: FormCustomValidator,
							private auth: AuthHttp) {
		toastService.setRoot(viewContainerRef);
		if (this.auth.getLoggedUsuario()) {
			this.model.usuario = this.auth.getLoggedUsuario().id;
		}
	}

	ngOnInit() {
		this.form = this.formValidator.createForm(this.model);
	}

	save() {
		// Trata imagens
		this.uploadService.commitUpload(this.model['foto']);

		if (this.form.valid) {
			this.toastService.showLoadingToast();
			this.model.fromFormGroup(this.form);
			console.log(this.model)
			this.service.insert(this.model).then( (result) => {
				this.alert.showAddSuccess();
				this.toastService.clearToasts();
			}).catch((err) => {
				console.log(err);
				this.alert.showAddError();
				this.toastService.clearToasts();
			});
		} else {
			this.shouldShowBadge = true;
			this.toastService.clearToasts();
			this.alert.showAddError();
		}
	}

	cancel() {
		this.list();
	}

	showFormErrorAlert() {
		this.alert.showAddError();
		this.formValidator.validateAllFormFields(this.form);
	}

	list() {
		this.router.navigate(['/contatos'], { relativeTo: this.route } );
	}
}
