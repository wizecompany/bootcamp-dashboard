import { Component, OnInit, ViewContainerRef, ViewChild, HostListener } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Contato } from '../../models/contatos.model';
import { ContatosService } from '../../services/contatos.service';

import { MaskService } from '../../../../core/services/masks.service';
import { UploadService } from '../../../../core/services/upload.service';
import { FormCustomValidator } from '../../../../core/services/formCustomValidator.service';

import { ToastService } from '../../../../core/services/toast.service';
import { NavProvider } from '../../../../layout/navigation.provider';

@Component({
		selector: 'app-contatos',
		templateUrl: './contatos-edit.component.html',
		styleUrls: ['./contatos-edit.component.scss'],
		animations: [routerTransition()]
})
export class ContatosEditComponent implements OnInit {
	@ViewChild('alert') private alert: any;
	@ViewChild('foto') private cnhImg: any;

	model: Contato = new Contato();

	private form: FormGroup;
	private shouldShowBadge = false;

	constructor(public router: Router,
							private formBuilder: FormBuilder,
							public route: ActivatedRoute,
							private service: ContatosService,
							public toastService: ToastService,
							public nav: NavProvider,
							public maskService: MaskService,
							private uploadService: UploadService,
							private vcr: ViewContainerRef,
							private formValidator: FormCustomValidator) {
		this.toastService.setRoot(vcr);
		this.model.fromAPI(this.nav.getParameters('model'));
		if (!this.nav.getParameters('model')) {
			this.list();
		}
	}

	ngOnInit() {
		this.form = this.formValidator.createForm(this.model);
	}

	getEmpresas() {

	}

	save() {
		// Trata imagens
		this.uploadService.commitUpload(this.model['foto']);

		if (this.form.valid) {
			this.toastService.showLoadingToast();
			this.model.fromFormGroup(this.form);
			this.service.update(this.model).then( (result) => {
				this.alert.showAddSuccess();
				this.toastService.clearToasts();
			}).catch((err) => {
				console.log(err);
				this.alert.showAddError();
				this.toastService.clearToasts();
			});
		} else {
			this.shouldShowBadge = true;
			this.toastService.clearToasts();
			this.alert.showAddError();
		}
	}

	showFormErrorAlert() {
		this.alert.showEditError();
		this.formValidator.validateAllFormFields(this.form);
	}

	cancel() {
		this.list();
	}

	list() {
		this.router.navigate(['/contatos'], { relativeTo: this.route });
	}

	confirmSwall() {
		this.alert.showEditConfirm();
	}
}
