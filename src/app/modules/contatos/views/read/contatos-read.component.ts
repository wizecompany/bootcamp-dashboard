import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';

import { BaseReadComponent } from '../../../../core/views/base-read.component';

import { Contato } from '../../models/contatos.model';

import { AuthHttp } from '../../../../core/services/auth.http';

import { NavProvider } from '../../../../layout/navigation.provider';

@Component({
		selector: 'app-contatos',
		templateUrl: './contatos-read.component.html',
		styleUrls: ['./contatos-read.component.scss'],
		animations: [routerTransition()]
})
export class ContatosReadComponent extends BaseReadComponent implements OnInit {
	model: Contato = new Contato();
	constructor(public nav: NavProvider,
							public router: Router,
							public authService: AuthHttp,
							public route: ActivatedRoute) {
		super(authService);

		if (!this.nav.getParameters('model')) {
			this.list();
		}

		this.model.fromAPI(this.nav.getParameters('model'));
	}

	ngOnInit() {
		super.ngOnInit();
	}

	edit() {
		this.nav.clearParameters();
		this.nav.setParameters('model', this.model);
		this.router.navigate(['/contatos/edit'], { relativeTo: this.route });
	}

	cancel() {
		this.list();
	}

	list() {
		this.router.navigate(['/contatos'], { relativeTo: this.route });
	}
}
