import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';
import { ToastOptions } from 'ng2-toastr';

import { Contato } from '../../models/contatos.model';
import { ContatosService } from '../../services/contatos.service';
import { ToastService } from '../../../../core/services/toast.service';

import { NavProvider } from '../../../../layout/navigation.provider';

import { AuthHttp } from '../../../../core/services/auth.http';
import { BaseListComponent } from '../../../../core/views/base-list.component';

@Component({
	selector: 'app-contatos',
	templateUrl: './contatos-list.component.html',
	styleUrls: ['./contatos-list.component.scss'],
	animations: [routerTransition()]
})
export class ContatosListComponent extends BaseListComponent implements OnInit {
	@ViewChild('alert') private alert: any;
	@ViewChild('pagination') private pagination: any;
	@ViewChild('datatable') private datatable: any;


	models: Contato[] = [];
	filteredModels: any[] = [];
	selectedModel: any;

	nomeSearch = '';

	filter: any = {};
	paging: any = { pageSize: 10 };
	pages: any = [];
	sort: string;
	pagingConfig = { column: 'createdAt', sort: 'DESC', filter: this.filter, paging: this.paging };

	tableResource;
	contadorItens = 0;

	pageSize = 10;

	collapseOpen = false;

	constructor(public router: Router,
		public route: ActivatedRoute,
		public toastr: ToastsManager,
		vcr: ViewContainerRef,
		public nav: NavProvider,
		private service: ContatosService,
		public authService: AuthHttp,
		public toastService: ToastService) {
		super(authService);

		this.toastService.setRoot(vcr);
	}

	ngOnInit() {
		super.ngOnInit();
		this.search();
	}

	applyFilter() {
		this.search();
	}

	search(value = this.pagingConfig) {
		// let sortString = value.column === '' ? '' : value.column + ' ' + value.sort;
		this.showLoading();
		this.service.all().then((result) => {
			console.log(result);
			this.models = result;
			// this.paging = result;
			// this.datatable.setPaging(result.paging);

			this.hideLoading();
		}).catch(err => {
			console.log(err);
			this.hideLoading();
		});
	}

	add() {
		this.router.navigate(['/contatos/add'], { relativeTo: this.route });
	}

	read(model) {
		this.nav.clearParameters();
		this.nav.setParameters('model', model);
		this.nav.setParameters('success', model);
		this.router.navigate(['/contatos/read'], { relativeTo: this.route });
	}

	edit(model) {
		this.nav.clearParameters();
		this.nav.setParameters('model', model);
		this.nav.setParameters('success', model);
		this.router.navigate(['/contatos/edit'], { relativeTo: this.route });
	}

	deleteAlert(model) {
		this.selectedModel = model;
		this.alert.showDeleteConfirm();
	}

	delete() {
		this.showLoading();
		this.service.delete(this.selectedModel).then(() => {
			this.hideLoading();
			this.alert.showDeleteSuccess();
		}).catch((err) => {
			this.hideLoading();
			this.alert.showDeleteError();
		});
	}

	showLoading() {
		this.toastService.showLoadingToast('Carregando...');
	}

	hideLoading() {
		this.toastService.clearToasts();
	}

	cleanFiltering() {
		this.paging = {};
		this.applyFilter();
	}

	changeCollapseState() {
		this.collapseOpen = !this.collapseOpen;
	}

	filterPressed(event) {
		if (event === 13) { this.applyFilter(); }
	}
}
