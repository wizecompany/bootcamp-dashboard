import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { AuthHttp } from '../services/auth.http';


export class BaseAddComponent implements OnInit {

	protected usuario: any = {};

	constructor(public authService: AuthHttp) {
		// constructor
	}

	ngOnInit() {
		this.usuario = this.authService.getLoggedUsuario();
	}
}
