import { Injectable } from '@angular/core';

@Injectable()
export class FilaProvider {

	private fila: {
		funcoes: any[],
		wait: number,
		contador: number,
	};

	constructor() {
	}

	public iniciarFila() {
		this.fila = {funcoes: [], wait: null, contador: null};
	}

	public getFila() {
		return this.fila;
	}

	public setWait(wait: number) {
		if (this.fila === null) {
			this.iniciarFila();
		}
		this.fila.wait = wait;
	}

	public setContador(contador: number) {
		if (this.fila === null) {
			this.iniciarFila();
		}
		this.fila.contador = contador;
	}

	public pushFila(funcao: any) {
		if (this.fila === null) {
			this.iniciarFila();
		}
		this.fila.funcoes.push(funcao);
	}

	public popFila() {
		this.fila.funcoes.pop();
	}

	public cleanFila() {
		this.fila = null;
	}

	public runFila() {
		try {
			if (this.fila === null) { throw new Error ('Fila não foi inicializada.'); }
			if (this.fila.wait === null) { throw new Error ('Tempo de espera para executar a proxima funcao nao foi definido'); }
			if (this.fila.contador === null) { throw new Error ('Contador não foi definido. Quantidade de funções que serão executadas antes de esperar'); }

			let i;
			let esperando = true;

			while (this.fila.funcoes.length > 0) {
				if (esperando) {
					for (i = 0; i < this.fila.contador && this.fila.funcoes.length > 0; i++) {
						this.fila.funcoes[i]();
					}
					esperando = false;
					setTimeout(() => {
						esperando = true;
					}, this.fila.wait);
				}
			}
		} finally {}
	}
}
