import { Injectable } from '@angular/core';
import { FormControl, FormGroup, AbstractControl, FormBuilder } from '@angular/forms';
import { ToastService } from './toast.service';
import { Validacoes } from './validacoes.service';

@Injectable()
export class FormCustomValidator {
	private alertsMsg: any;

	constructor(private toast: ToastService, private formBuilder: FormBuilder) {

	}


	public validateAllFormFields(formGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field);
			if (control instanceof FormControl && control.enabled) {
				control.markAsTouched({onlySelf: true});
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control);
			}
		});
	}

	public isFieldValid(field: string, form: FormGroup) {
		return !form.get(field).valid && form.get(field).touched;
	}

	public displayFieldCss(field: string, form: FormGroup) {
		return{
			'has-error': this.isFieldValid(field, form),
			'has-valid': !this.isFieldValid(field, form) && form.get(field).touched
		};
	}

	public createForm(model: any) {
		return this.formBuilder.group(model.toFormGroup());
	}

	public createFormGroup(form: any) {
		return this.formBuilder.group(form);
	}

	public isAllFieldsValid(formGroup) {
		for (let field of Object.keys(formGroup.controls)) {
			const control = formGroup.get(field);
			if (control.valid === false && control.enabled) {
				return false;
			}
		}
		return true;
	}

	public allFormIsValid(formArray) {
		for (let form of formArray) {
			if (form.valid === false) {
				console.log(form);
				return false;
			}
		}
		return true;
	}

	public countFormErros(formGroup) {
		return Object.keys(formGroup.controls).filter(field => !formGroup.get(field).valid && formGroup.get(field).enabled).length;
	}

	public formPatchValue(field, form, value) {
		form.get(field).patchValue(value);
	}

	public invalidFieldsCounter(form, fields: any[] = []) {
		return fields.filter(field => !form.get(field).valid && form.get(field).enabled).length;
	}
}
