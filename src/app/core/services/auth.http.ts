import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { AppConfig } from '../../../environments/environment';

import { StorageService } from './storage.service';

@Injectable()
export class AuthHttp {

	private serviceUrl: string = AppConfig.serviceUrl;
	private token: any = null;
	private loggedIn = false;
	private loggedUser: any = null;

	constructor(
		private http: Http,
		private localStorage: StorageService,
		private router: Router
	) {
		let usuario = JSON.parse(this.localStorage.getData('usuario'));
		if (usuario !== null) {
			this.token = usuario.token;
			this.loggedIn = usuario.active;
			this.loggedUser = usuario.usuario;
		} else {
			console.log(`localStorage sem usuario`);
		}
	}

	public post(url: string, data: any) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Content-Type', 'application/json');
			header.append('Authorization', 'Bearer ' + String(this.token));
			const options = new RequestOptions({ headers: header });
			this.http.post(this.serviceUrl + url, data, options).subscribe((result: any) => {
				resolve(result.json());
			},
			err => {
				console.log(err);
				reject(err);
			});
		});
	}

	public noAuthPost(url: string, data: any) {
		return new Promise((resolve, reject) => {
			this.http.post(this.serviceUrl + url, data).subscribe((result: any) => resolve(result.status),
			err => reject(err));
		});
	}

	public put(url: string, data: any) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Authorization', 'Bearer ' + String(this.token));
			header.append('Content-Type', 'application/json');
			const options = new RequestOptions({ headers: header });
			this.http.put(this.serviceUrl + url, data, options).subscribe((result: any) => {
				resolve(result.json());
			},
			err => {
				console.log(err);
				reject(err);
			});
		});
	}

	public delete(url: string) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Authorization', 'Bearer ' + String(this.token));
			header.append('Content-Type', 'application/json');
			const options = new RequestOptions({ headers: header });

			this.http.delete(this.serviceUrl + url, options).subscribe((data: any) => {
				resolve(data);
			},
			err => {
				console.log(err);
				reject(err);
			});
		});
	}

	public get(url: string) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Authorization', 'Bearer ' + String(this.token));
			header.append('Content-Type', 'application/json');
			const options = new RequestOptions({ headers: header });

			this.http.get(this.serviceUrl + url, options).subscribe((data: any) => {
				resolve(data.json());
			},
			(err) => {
				console.log(err);
				reject(err);
			});
		}).catch((err) => {
			console.log('ERROR', err);
		});
	}

	public saveToken(token) {
		this.token = token;
		this.loggedIn = true;
	}

	public auth(email: string, password: string) {
		const data  = { email: email, senha: password };
		return new Promise((resolve, reject) => {
			this.http.post(this.serviceUrl + 'usuario/login/', data).subscribe((result: any) => {
					const json: any = result.json();
					this.loggedUser = json.user;
					this.setLoggedUsuario(json.user);
					if (json.token) {
						this.saveToken(json.token);
						resolve(result.json());
					}
			}, (err) => {
					console.log('Error on Auth');
					reject(err);
				}
			);
		});
	}

	public validUser() {
		return this.token || !!this.localStorage.getData('usuario');
	}

	public logout() {
		this.loggedIn = false;
		this.token = null;
		this.localStorage.removeData('usuario');
	}

	public getLoggedUsuario() {
		return this.loggedUser;
	}

	public getUserToken(): string {
		return this.token;
	}

	public setLoggedUsuario(usuario: any) {
		this.localStorage.storeData('usuario', JSON.stringify(usuario));
		this.loggedUser = usuario;
	}
}
