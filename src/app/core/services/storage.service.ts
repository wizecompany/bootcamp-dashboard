import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

		constructor() {}

		public storeData(key: string, data: string) {
			localStorage.setItem('usuario', data);
		}

		public getData(key: string) {
			return localStorage.getItem(key);
		}

		public removeData(key: string) {
			return new Promise((resolve, reject) => {
				localStorage.removeItem(key);
				localStorage.getItem(key) === null
					? resolve()
					: reject(`localStorage ${key} não removido`);
			});
		}
}
