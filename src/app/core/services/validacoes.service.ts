import { FormControl, FormGroup, AbstractControl, FormBuilder } from '@angular/forms';

export class Validacoes {
	public static validarCPF(control: AbstractControl) {
		let cpf = control.value;
		if (cpf === undefined || cpf === null) {
			return { cpfValid: false };
		}

		let cpfClean = cpf.replace(/\./g, '');
		cpfClean     = cpfClean.replace('-', '');

		let firstDigito = cpfClean.substring(9, 10);
		let secDigito   = cpfClean.substring(10, 11);

		let primDigitoValido = false;
		let secDigitoValido = false;

		let primParte = cpfClean.substring(0, 9);
		let secParte  = cpfClean.substring(0, 10);
		let soma      = 0;

		let j = 0;
		let number;

		let resto;
		let result;

		for (let i = 10; i >= 2; i--) {
			number = primParte.substring(j, j + 1);
			soma   = soma + parseInt(number, 10) * i;
			j++;
		}

		resto = soma % 11;
		result = 11 - resto;
		if (result > 9 && firstDigito === '0') {
			primDigitoValido = true;
		} else if (result <= 9 && parseInt(firstDigito, 10) === result) {
			primDigitoValido = true;
		} else {
			return { cpfValid: false };
		}

		// segundo digito
		j = 0;
		soma = 0;
		for (let i = 11; i >= 2; i--) {
			number = secParte.substring(j, j + 1);
			soma = soma + parseInt(number, 10) * i;
			j++;
		}

		resto = soma % 11;
		result = 11 - resto; if (result > 9 && secDigito === '0') {
			secDigitoValido = true;
		} else if (result <= 9 && parseInt(secDigito, 10) === result) {
			secDigitoValido = true;
		} else {
			return { cpfValid: false };
		}

		if (secDigitoValido && primDigitoValido) {
			return null;
		}

	}

	public static validarCNPJ(control: AbstractControl) {
		let cnpj = control.value;
		if (cnpj === undefined) {
			return { cnpjValid: false };
		}

		let cnpjClean = cnpj.replace(/\./g, '');
		cnpjClean     = cnpjClean.replace('/', '');
		cnpjClean     = cnpjClean.replace('-', '');

		let firstDigito = cnpjClean.substring(12, 13);
		let secDigito   = cnpjClean.substring(13, 14);

		let primDigitoValido = false;
		let secDigitoValido = false;

		let primParte = cnpjClean.substring(0, 12);
		let secParte  = cnpjClean.substring(0, 13);
		let soma      = 0;

		let j = 0;
		let number;

		let resto;
		let result;

		for (let i = 5; i >= 2; i--) {
			number = primParte.substring(j, j + 1);
			soma = soma + parseInt(number, 10) * i;
			j++;
		}

		for (let i = 9; i >= 2; i--) {
			number = primParte.substring(j, j + 1);
			soma = soma + parseInt(number, 10) * i;
			j++;
		}

		resto = soma % 11;
		result = 11 - resto;
		if (resto < 2 && firstDigito === '0') {
			primDigitoValido = true;
		} else if (resto >= 2 && parseInt(firstDigito, 10) === result) {
			primDigitoValido = true;
		} else {
			return { cnpjValid: false };
		}

		// segundo digito
		j = 0;
		soma = 0;
		for (let i = 6; i >= 2; i--) {
			number = secParte.substring(j, j + 1);
			soma = soma + parseInt(number, 10) * i;
			j++;
		}

		for (let i = 9; i >= 2; i--) {
			number = secParte.substring(j, j + 1);
			soma = soma + parseInt(number, 10) * i;
			j++;
		}

		resto = soma % 11;
		result = 11 - resto;

		if (resto < 2 && secDigito === '0') {
			secDigitoValido = true;
		} else if (resto >= 2 && parseInt(secDigito, 10) === result) {
			secDigitoValido = true;
		} else {
			return { cnpj: false };
		}

		if (secDigitoValido && primDigitoValido) {
			return null;
		}
	}

	public static validarQtdResponsaveis(control: AbstractControl) {
		let value = control.value;
		if (value === undefined || value === null) {
			return {qtdResponsaveisValid : false};
		}
		if (value > 0) {
			return null;
		} else {
			return {qtdResponsaveisValid : false};
		}

	}
}
