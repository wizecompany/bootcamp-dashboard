import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';

@Injectable()
export class UploadService {

	location: 'string';

	toDelete = [];

	// Dados para autenticacao no bucket
	private bucket = new S3({
		accessKeyId: 'AKIAJOC7FWLNILG4NS4Q',
		secretAccessKey: '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',
		region: 'us-east-1'
	});

	constructor() { }

	uploadArquivo (arquivo) {
		return new Promise((resolve, reject) => {

			// Parametros de arquivo
			const params = {
				Bucket: 'wizebin-dashboard',
				Key: `${this.milisegundosAgora()}.${this.extensaoArquivo(arquivo.name)}`,
				Tagging: 'delete=true',
				Body: arquivo
			};
			this.bucket.upload(params).promise()
				.then(dados => resolve(dados.Location))
				.catch(erro => reject(erro));
		});
	}

	// Deve ser chamado ao salvar a url no modelo
	commitUpload (url: String) {
		if (url === undefined) { return; }

		let params = {
			Bucket: 'wizebin-upload',
			Key: this.nomeArquivo(url),
			Tagging: {
				TagSet: []
			}
		};

		// Remove tag de exclusao automatica
		this.bucket.deleteObjectTagging(params, (erro, dados) => erro ? false : true);

		// Marca itens removidos para exclusao
		if (!!this.toDelete.length) {
			params.Tagging.TagSet.push({
				Key: 'delete',
				Value: 'true'
			});
			this.bucket.putObjectTagging(params, (erro, dados) => erro ? false : true);
		}
	}

	milisegundosAgora = () => (new Date).getTime();

	nomeArquivo = arquivo => this.recebeFinal(arquivo, '/');
	extensaoArquivo = arquivo => this.recebeFinal(arquivo, '.');

	recebeFinal = (texto, caractere) => texto.substring(texto.lastIndexOf(caractere) + 1);
}
