// import { Injectable } from '@angular/core';
// import { AuthHttp } from './auth.http';
// import { Account } from '../models/account.model';
//
// @Injectable()
// export class AccountService {
// 		private baseUrl = 'account/';
//
//     constructor(private authHttp: AuthHttp) {
//
// 		}
//
// 		all():Promise<any> {
// 			return this.authHttp.get(this.baseUrl);
// 		}
//
// 		insert(model : Account):Promise<any> {
// 			return this.authHttp.post(this.baseUrl, model);
// 		}
//
// 		update(model : Account):Promise<any> {
// 			return this.authHttp.put(this.baseUrl + model.id, model);
// 		}
//
// 		delete(model : Account):Promise<any> {
// 			return this.authHttp.delete(this.baseUrl + model.id);
// 		}
// }
