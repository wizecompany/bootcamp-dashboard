import { Injectable } from '@angular/core';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';
import { ToastOptions } from 'ng2-toastr';

@Injectable()
export class ToastService {
		private toast: any;
		constructor(public toastManager: ToastsManager) {
		}

		showSuccessToast(title: string = 'Sucesso', msg: string = 'Concluído') {
			this.toastManager.success(msg, title, {toastLife: 3000, showCloseButton: true, animate: 'flyLeft'});
		}

		showWarningToast(title: string = 'Atenção', msg: string = 'Cuidado') {
			this.toastManager.warning(msg, title, {toastLife: 3000, showCloseButton: true, animate: 'flyLeft'});
		}

		showErrorToast(title: string = 'Erro', msg: string = 'Erro') {
			this.toastManager.error(msg, title, {toastLife: 3000, showCloseButton: true, animate: 'flyLeft'});
		}

		showLoadingToast(msg: string = 'Carregando...') {
			setTimeout(() => {
				this.toastManager.custom('<div class="loader"></div>', msg, {toastLife: 3600000, enableHTML: true, showCloseButton: true, animate: 'flyLeft'});
			}, 100);
		}

		clearToasts() {
			this.toastManager.clearAllToasts();
		}

		setRoot(vcr: any) {
			this.toastManager.setRootViewContainerRef(vcr);
		}

}
