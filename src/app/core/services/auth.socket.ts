import { Injectable } from '@angular/core';
import { AppConfig } from '../../../environments/environment';
import { AuthHttp } from './auth.http';

import * as socketIOClient from 'socket.io-client';
import * as sailsIOClient from 'sails.io.js';

let io;

@Injectable()
export class AuthSocket {

	private serviceUrl: string = AppConfig.serviceUrl;

	constructor(public authService: AuthHttp) {
		if (socketIOClient.sails) {
			io = socketIOClient;
		} else {
			io = sailsIOClient(socketIOClient);
		}
		io.sails.url = this.serviceUrl;
		io.sails.headers = { 'Authorization': 'Bearer ' + this.authService.getUserToken() };
	}

	public subscribe(url: string, event: string, callback: any) {
		io.socket.get(url, () => {
			io.socket.on(event, entry => {
				callback(entry);
			});
		});
	}

	public unsubscribe(url: string, callback: any) {
		io.socket.get(url, () => {
			io.socket.removeAllListeners();
			if (callback) {
				callback();
			}
		});
	}
}
