
export class ModuloFuncao {
	static ADD  		= 'add';
	static READ 		= 'read';
	static EDIT 		= 'edit';
	static DELETE 	= 'delete';
	static SEARCH 	= 'search';
	static EXPORT 	= 'export';
	static PRINT 		= 'print';
	static REPORT 	= 'report';

	id: string;
	nome: string;

	constructor(id: string, nome: string) {
		this.id 	= id;
		this.nome = nome;
	}

	static all() {
		let functions: any = [];

		functions.push(new ModuloFuncao(ModuloFuncao.ADD, 			'Adicionar'));
		functions.push(new ModuloFuncao(ModuloFuncao.READ, 			'Ler'));
		functions.push(new ModuloFuncao(ModuloFuncao.EDIT, 			'Editar'));
		functions.push(new ModuloFuncao(ModuloFuncao.DELETE, 		'Deletar'));
		functions.push(new ModuloFuncao(ModuloFuncao.SEARCH, 		'Buscar'));
		functions.push(new ModuloFuncao(ModuloFuncao.EXPORT, 		'Exportar'));
		functions.push(new ModuloFuncao(ModuloFuncao.PRINT, 		'Imprimir'));
		functions.push(new ModuloFuncao(ModuloFuncao.REPORT, 		'Relatórios'));

		return functions;
	}
}
