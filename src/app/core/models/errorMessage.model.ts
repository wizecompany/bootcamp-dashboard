import { FormGroup } from '@angular/forms';
export class ErrorMessage {
	public placement = 'top-left';
	public trigger = 'focus:blur';
	// public triggerNgSelect = 'click';

	constructor() {}

	public getError(field: string, form, label: string = field) {
		if (form !== null || form !== undefined) {
			let error = null;
			if (this.isFieldValid(field, form)) {
				error = form.get(field).errors;
				if (error !== null) {
					return this.getAlertsMsg(label, Object.keys(error)[0]);
				} else {
					return '';
				}
			}
		}
	}

	public isFieldValid(field: string, form: FormGroup) {
		return !form.get(field).valid && form.get(field).touched;
	}

	private getAlertsMsg(field: string, error: string) {
		const textos = {
			required: `Campo '${field}' é obrigatório!`,
			minlength: `Campo '${field}' não atingiu o limite mínimo de caracteres!`,
			maxlength: `Campo '${field}' ultrapassou o limite de caracteres permitido!`,
			pattern: `Campo '${field}' não coincide com a regra!`,
			cpfValid: `Campo cpf inválido!`,
			cnpjValid: `Campo cnpj inválido!`,
			qtdResponsaveisValid: `Deve ter ao menos um responsável cadastrado!`,
			email: `E-mail inválido`,
		};
		return textos[error];
	}
}
