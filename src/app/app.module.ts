import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';

// import { DlDateTimePickerDateModule } from 'angular-bootstrap-datetimepicker';
import { TextMaskModule } from 'angular2-text-mask';
// Components
// import { ToastModule, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthHttp } from './core/services/auth.http';
import { AuthGuard } from './shared';
import { FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';

import { ToastService } from './core/services/toast.service';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ToastOptions } from 'ng2-toastr';

import { StorageService } from './core/services/storage.service';
import { UploadService } from './core/services/upload.service';
import { UploadModule } from './shared/modules/upload/upload.module';
import { FormCustomValidator } from './core/services/formCustomValidator.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ErrorMessage } from './core/models/errorMessage.model';
import { AuthSocket } from './core/services/auth.socket';
import { ErrorDisplayComponent } from './shared/components/error-display/error-display.component';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
	// for development
	// return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-5/master/dist/assets/i18n/', '.json');
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	imports: [
		CommonModule,
		BrowserModule,
		FormsModule,
		HttpModule,
		BrowserAnimationsModule,
		HttpClientModule,
		ToastModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			}
		}),
		// DlDateTimePickerDateModule,
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyDZoxwU6v3E09EZz_BXeUYH2kkxH01uDQM'
		}),
		AppRoutingModule,
		TextMaskModule,
		ReactiveFormsModule,
		NgbModule.forRoot()
	],
	declarations: [
		AppComponent,
		ErrorDisplayComponent,
		// AlertModalComponent
	],
	providers: [
		AuthGuard,
		AuthHttp,
		AuthSocket,
		ToastService,
		ToastOptions,
		UploadService,
		StorageService,
		FormCustomValidator,
		ErrorMessage
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
