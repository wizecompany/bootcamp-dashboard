import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';

import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SidebarService } from './components/services/sidebar.service';
import { HeaderComponent } from './components/header/header.component';
import { NavProvider } from './navigation.provider';
import { AlertsModule } from '../shared';

@NgModule({
		imports: [
				CommonModule,
				LayoutRoutingModule,
				TranslateModule,
				NgbDropdownModule.forRoot(),
				FormsModule,
				ReactiveFormsModule,
				AlertsModule
				// DashboardModule,
				// ClienteModule
		],
		declarations: [LayoutComponent, SidebarComponent, HeaderComponent],
		providers: [
			NavProvider,
			SidebarService
		]
})
export class LayoutModule {}
