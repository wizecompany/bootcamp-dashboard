import { Component, OnInit } from '@angular/core';

import { AuthHttp } from '../core/services/auth.http';
import { SidebarService } from './components/services/sidebar.service';

@Component({
		selector: 'app-layout',
		templateUrl: './layout.component.html',
		styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
		private username = 'Usuário';

		constructor(private auth: AuthHttp, public sidebarService: SidebarService) {
			let user = this.auth.getLoggedUsuario();
			if (user) {
				this.username = user.nome;
			}
		}

		ngOnInit() {}
}
