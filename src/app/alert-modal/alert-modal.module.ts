import { AlertModalComponent } from './alert-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
		imports: [
			CommonModule,
		],
		declarations: [
			AlertModalComponent
		],
		entryComponents: [
			AlertModalComponent
		]
})
export class AlertModalModule {}
