import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'alert-modal',
	templateUrl: './alert-modal.component.html',
	styleUrls: ['./alert-modal.component.scss']
})
export class AlertModalComponent implements OnInit {
	title = 'Header Modal';

	constructor() { }

	ngOnInit() {
	}

}
